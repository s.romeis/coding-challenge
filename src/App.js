// @flow

import React from 'react';
import {connect} from 'react-redux';
import './App.css';

import Header from './components/Header/Header';
import Searchbar from './components/Searchbar/Searchbar';
import {Info} from './components/Info/Info';
import Map from './components/Map/Map';
import {fetchAirports, setAirport} from "./actions/searchReducer";


//collect alle components and renders them to the application
//is the only smart component of the application
class App extends React.Component {

    render() {
        const {search} = this.props;
        return (
            <div className="App">
                <div>
                    <Map
                        initialPosition={search.mapInitialPos}
                        airportToView={search.airportToView}
                    />
                    <Info
                        airportToView={search.airportToView}
                    />
                </div>
                <Header>
                    <Searchbar
                        suggestions={search.airportsByAutosuggest}
                        onSuggestionSelected={this.props.setAirport}
                        onSuggestionsFetchRequested={this.props.fetchAirports}
                    />
                </Header>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        search: state.search
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setAirport: (airport) => {
            dispatch(setAirport(airport));
        },
        fetchAirports: (suggestVal) => {
            dispatch(fetchAirports(suggestVal));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);