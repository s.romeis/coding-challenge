// @flow
import api from 'axios';

api.defaults.baseURL = 'https://sandbox.paxlife.aero';

let lastRequestId = null;

export function fetchAirports(val: string): Object {
    // Cancel the previous request
    if (lastRequestId !== null) {
        clearTimeout(lastRequestId);
    }

    return (dispatch) => {
        return lastRequestId = setTimeout(() => {
            dispatch({
                type: "FETCH_AIRPORTS_PENDING"
            })
            api.get("/api/search/" + val)
                .then((response) => {
                    dispatch({
                        type: "FETCH_AIRPORTS_FULFILLED",
                        payload: response.data
                    });
                })
                .catch((err) => {
                    dispatch({
                        type: "FETCH_AIRPORTS_ERROR",
                        payload: 'rest'
                    })
                })
        }, 500)
    }
}

export function setAirport(airport: Object) {
    return (dispatch: Object) => {
        dispatch({
            type: "SET_AIRPORT",
            payload: airport
        });
    }
}
