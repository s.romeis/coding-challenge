// @flow

import React from 'react';
import PropTypes from 'prop-types';
import google from 'google-maps';
import './Map.css';

class Map extends React.Component {

    componentDidMount() {
        google.KEY = 'AIzaSyAD9a-xf1fho75or5BNv_LzCuOo5hSse2c';

        //set initial pos as props
        const {lat, lng} = this.props.initialPosition;

        //init map und marker
        google.load(google => {
            this.map = new google.maps.Map(this.refs.map, {
                center: {lat, lng},
                zoom: 10
            });

            this.marker = new google.maps.Marker({
                position: {lat, lng},
                map: this.map
            });
        })
    }

    componentWillReceiveProps(nextProps) {
        //pan to new pos when mapStateToProps is called
        const airport = nextProps.airportToView;

        if(Object.keys(airport).length > 0) {
            this.map.panTo({
                lat: airport.latitude,
                lng: airport.longitude
            });

            google.load(google => {
                //remove old marker
                this.marker.setMap(null);
                //create new Marker
                this.marker = new google.maps.Marker({
                    position: {
                        lat: airport.latitude,
                        lng: airport.longitude
                    },
                    map: this.map
                });
            })
        }
    }

    render() {
        return (
            <div className="Map">
                <div ref="map" style={{width: '100%', height: '100%'}}>
                    I should be a map!
                </div>
            </div>
        );
    }
}

Map.propTypes = {
    initialPosition: PropTypes.object.isRequired,
    position: PropTypes.object
};

export default Map;