import {createStore, combineReducers, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import searchReducer from "./reducer/searchReducer";

export default createStore(
    combineReducers({
        search: searchReducer
    }),
    {},
    applyMiddleware(promise(), thunk, logger)
);